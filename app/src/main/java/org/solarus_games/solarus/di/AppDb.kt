package org.solarus_games.solarus.di

import androidx.room.Database
import androidx.room.RoomDatabase
import org.solarus_games.solarus.recent.RecentlyPlayed
import org.solarus_games.solarus.recent.RecentlyPlayedDao

@Database(entities = [RecentlyPlayed::class], version = 1, exportSchema = false)
abstract class AppDb : RoomDatabase() {
    abstract fun recentlyPlayedDao(): RecentlyPlayedDao
}