package org.solarus_games.solarus.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.solarus_games.solarus.BuildConfig
import org.solarus_games.solarus.common.Constants.SOLARUS_BASE_URL
import org.solarus_games.solarus.recent.RecentlyPlayedDao
import org.solarus_games.solarus.remote.RemoteService
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Singleton
    @Provides
    fun provideAppDb(application: Application): AppDb {
        return Room.databaseBuilder(
            application,
            AppDb::class.java,
            "app_db"
        ).build()
    }

    @Singleton
    @Provides
    fun provideRecentlyPlayedDao(db: AppDb): RecentlyPlayedDao {
        return db.recentlyPlayedDao()
    }


    @Singleton
    @Provides
    fun provideRemoteService(): RemoteService {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        )
        val client = OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(SOLARUS_BASE_URL)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        return retrofit.create(RemoteService::class.java)
    }
}