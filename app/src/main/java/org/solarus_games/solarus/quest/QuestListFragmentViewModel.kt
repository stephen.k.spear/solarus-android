package org.solarus_games.solarus.quest

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.solarus_games.solarus.preferences.PreferencesManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuestListFragmentViewModel @Inject constructor(
    val preferencesManager: PreferencesManager,
    val questManager: QuestManager
) : ViewModel() {

    var state = MutableStateFlow(QuestsState(isLoading = true))
        private set

    init {
        getQuestsPage()
    }

    private fun getQuestsPage() {
        val wrapperFlow = combine(
            questManager.quests,
            preferencesManager.sortDescendingFlow(),
        ) { quests, sortDesc -> DirSortWrapper(quests, sortDesc) }

        wrapperFlow.onEach { wrapper ->
            with(wrapper) {
                var result = quests
                result = if (sortDescending) {
                    result.sortedByDescending { q -> q.title }
                } else {
                    result.sortedBy { q -> q.title }
                }
                state.emit(QuestsState(quests = result))
            }
        }.launchIn(viewModelScope)
    }

    fun onQuestSelected(context: Context, quest: Quest) {
        questManager.onQuestDetailClick(context, quest)
    }

    fun refreshQuests() {
        viewModelScope.launch {
            questManager.refreshQuests()
        }
    }

}

data class DirSortWrapper(
    val quests: List<Quest> = emptyList(),
    val sortDescending: Boolean,
)

data class QuestsState(
    val isLoading: Boolean = false,
    val quests: List<Quest> = emptyList(),
    val error: String = ""
)