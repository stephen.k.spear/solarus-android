package org.solarus_games.solarus.quest

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.map
import org.solarus_games.solarus.BaseQuestDetailActivity
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.QuestProperties
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.utils.openUrlInExternalBrowser
import javax.inject.Inject

@AndroidEntryPoint
class QuestDetailActivity : BaseQuestDetailActivity() {

    @Inject
    lateinit var viewModel: QuestDetailActivityViewModel

    override fun getQuestManager() = viewModel.questManager

    override fun onPlayClick() {
        viewModel.launchQuest(this)
        finish()
    }

    override fun onUpdateClick() {
        viewModel.downloadGame(this@QuestDetailActivity)
    }


    override fun getQuestTitle() = viewModel.quest.map { it?.title.orEmpty() }

    override fun getQuestDescription() = viewModel.quest.map { it?.shortDescription.orEmpty() }
    override fun hasUpdate() = viewModel.hasUpdate

    @Composable
    override fun GetHeaderActions() {
        val hasUpdate by viewModel.hasUpdate.collectAsState(initial = false)
        Box {
            var menuExpanded by remember { mutableStateOf(false) }
            IconButton(onClick = { menuExpanded = !menuExpanded }) {
                Icon(
                    painter = painterResource(id = R.drawable.menu_dots),
                    contentDescription = "Menu Dots",
                    tint = SecondaryTextLight
                )
            }
            DropdownMenu(
                expanded = menuExpanded,
                onDismissRequest = { menuExpanded = false },
                modifier = Modifier.background(PrimaryLight)
            ) {
                if (hasUpdate) {
                    DropdownMenuItem(
                        text = {
                            Text(stringResource(R.string.update))
                        },
                        onClick = {
                            menuExpanded = false
                            onUpdateClick()
                        },
                    )
                }
                DropdownMenuItem(
                    text = {
                        Text(stringResource(R.string.add_shortcut))
                    },
                    onClick = {
                        menuExpanded = false
                        viewModel.quest.value?.let {
                            addShortcut(it)
                        }
                    },
                )
                DropdownMenuItem(
                    text = {
                        Text(stringResource(R.string.delete))
                    },
                    onClick = {
                        menuExpanded = false
                        viewModel.deleteQuest()
                        finish()
                    },
                )
            }
        }
    }

    @Composable
    override fun GetThumbnailView() {
        val quest by viewModel.quest.collectAsState(null)
        quest?.let {
            QuestIconView(
                Modifier
                    .fillMaxWidth()
                    .defaultMinSize(minHeight = 200.dp),
                it,
                true
            ) {}
        }
    }

    override fun setup() {
        val path = requireNotNull(intent.getStringExtra(ARG_PATH))
        val quest = requireNotNull(viewModel.questManager.fromPath(path))
        viewModel.updateQuestState(quest)
        viewModel.collectRemoteDataForId(intent.getStringExtra(ARG_REMOTE_ID))
    }

    override fun getProperties() = viewModel.quest.map { quest ->
        mutableListOf<QuestProperties>().also { props ->
            quest?.let {
                with(it) {
                    if (author.isNotEmpty()) props.add(QuestProperties.DEVELOPER)
                    if (releaseDate.isNotEmpty()) props.add(QuestProperties.RELEASE_DATE)
                    if (version.isNotEmpty()) props.add(QuestProperties.VERSION)
                    if (website.isNotEmpty()) props.add(QuestProperties.WEBSITE)
                }
            }
        }
    }


    @Composable
    override fun GetPropertyView(modifier: Modifier, property: QuestProperties) {
        val quest by viewModel.quest.collectAsState(null)
        quest?.let {
            with(it) {
                when (property) {
                    QuestProperties.DEVELOPER -> QuestItemRow(
                        modifier,
                        property = R.string.developer,
                        value = author
                    )

                    QuestProperties.RELEASE_DATE -> QuestItemRow(
                        modifier,
                        property = R.string.release_date,
                        value = releaseDate
                    )

                    QuestProperties.VERSION -> QuestItemRow(
                        modifier,
                        property = R.string.version,
                        value = version
                    )


                    QuestProperties.WEBSITE -> QuestItemContentRow(modifier, property = R.string.website) {
                        LinkItem {
                            openUrlInExternalBrowser(website)
                        }
                    }

                    else -> {}
                }
            }
        }
    }

    companion object {
        private const val ARG_PATH = "path"
        private const val ARG_REMOTE_ID = "remote_id"

        fun startActivity(context: Context, quest: Quest) {
            val intent = Intent(context, QuestDetailActivity::class.java).also {
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            intent.putExtra(ARG_PATH, quest.path)
            intent.putExtra(ARG_REMOTE_ID, quest.remoteId)
            context.startActivity(intent)
        }
    }
}