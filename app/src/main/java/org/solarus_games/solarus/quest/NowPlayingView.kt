package org.solarus_games.solarus.quest

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryText
import org.solarus_games.solarus.theme.SolarusTypography

@Composable
fun NowPlayingView(
    modifier: Modifier,
    manager: QuestManager
) {
    val quest by manager.currentQuest.collectAsState(null)
    val screenShot by manager.screenCapture.collectAsState(null)
    quest?.let {
        with(it) {
            Column(
                modifier
                    .background(PrimaryLight)
                    .padding(horizontal = 20.dp, vertical = 10.dp),
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {

                Row(
                    Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(20.dp)
                ) {

                    Column(modifier = Modifier.weight(1f), verticalArrangement = Arrangement.spacedBy(5.dp)) {
                        Text(
                            stringResource(id = R.string.currently_playing),
                            style = SolarusTypography.bodyLarge.copy(fontWeight = FontWeight.W300),
                            color = SecondaryText
                        )
                        Text(
                            title,
                            style = SolarusTypography.bodyLarge.copy(fontWeight = FontWeight.Bold),
                            color = SecondaryLight,
                            maxLines = 2,
                            overflow = TextOverflow.Ellipsis
                        )
                    }

                    screenShot?.let { screen ->
                        Image(
                            bitmap = screen.asImageBitmap(),
                            contentDescription = "Screenshot",
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .clip(RoundedCornerShape(10.dp))
                                .size(80.dp, 60.dp)
                        )
                    }
                }

                Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {

                    Button(onClick = {
                        manager.onQuestQuitRequest()
                    }, colors = ButtonDefaults.buttonColors(containerColor = SecondaryText)) {
                        Image(
                            painter = painterResource(id = R.drawable.stop), contentDescription = "Stop",
                            modifier = Modifier
                                .size(24.dp),
                            colorFilter = ColorFilter.tint(Color.White)
                        )

                        Text(stringResource(id = R.string.quit_quest), modifier.padding(start = 5.dp))
                    }

                    Spacer(modifier = Modifier.width(20.dp))

                    Button(onClick = {
                        manager.onQuestResumeRequest(it)
                    }, colors = ButtonDefaults.buttonColors(containerColor = Secondary)) {
                        Image(
                            painter = painterResource(id = R.drawable.play), contentDescription = "Resume",
                            modifier = Modifier
                                .size(24.dp),
                            colorFilter = ColorFilter.tint(Color.White)
                        )

                        Text(stringResource(id = R.string.resume_quest), modifier.padding(start = 5.dp))
                    }

                }
            }
        }
    }
}