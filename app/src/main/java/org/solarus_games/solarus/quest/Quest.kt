package org.solarus_games.solarus.quest

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.regex.Pattern

/**
 * Create a valid quest with all info
 *
 * @param path             the file/folder path
 * @param title            quest title
 * @param shortDescription quest short description
 * @param longDescription  long description
 * @param version          version of the quest
 * @param author           author of the quest
 * @param logo             logo of the quest or placeholder logo
 * @param icon             icon of the quest or placeholder icon
 */
data class Quest constructor(
    val path: String,
    val title: String,
    val shortDescription: String,
    val longDescription: String,
    val version: String,
    val format: String,
    val author: String,
    val releaseDate: String,
    val website: String,
    val logo: Bitmap? = null,
    val icon: Bitmap? = null,
    val remoteId: String? = null,
)