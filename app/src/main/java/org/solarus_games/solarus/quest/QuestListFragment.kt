package org.solarus_games.solarus.quest

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.BaseQuestListFragment
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Center
import org.solarus_games.solarus.common.Constants
import org.solarus_games.solarus.common.Constants.COLUMN_PADDING
import org.solarus_games.solarus.common.Constants.LARGE_CARD_CORNERS
import org.solarus_games.solarus.preferences.FolderPreferenceFragment
import org.solarus_games.solarus.theme.SolarusTypography
import javax.inject.Inject


@AndroidEntryPoint
class QuestListFragment : BaseQuestListFragment<Quest>() {

    @Inject
    lateinit var viewModel: QuestListFragmentViewModel

    override val getHeaderStringRes = R.string.games

    override fun getPreferencesManager() = viewModel.preferencesManager
    override fun handleRefresh() = viewModel.refreshQuests()

    override fun shouldFilter(search: String, item: Quest) = item.title.contains(search, true)

    @Composable
    override fun GetContent() {
        val questState by viewModel.state.collectAsState(initial = QuestsState(isLoading = true))
        with(questState) {
            when {
                isLoading -> Center(modifier = Modifier.fillMaxSize()) {
                    CircularProgressIndicator()
                }

                else -> QuestListView(quests)
            }
        }
    }

    @Composable
    override fun GetItemView(item: Quest) {
        val context = LocalContext.current
        val useGrid by viewModel.preferencesManager.useGridViewFlow().collectAsState(initial = false)
        if (useGrid) {
            QuestIconView(
                modifier = Modifier
                    .padding(horizontal = COLUMN_PADDING.dp)
                    .clip(RoundedCornerShape(LARGE_CARD_CORNERS.dp))
                    .fillMaxWidth()
                    .height(200.dp), quest = item, true
            ) {
                viewModel.onQuestSelected(context, item)
            }
        } else {
            QuestColumnView(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp), quest = item
            ) {
                viewModel.onQuestSelected(context, item)
            }
        }
    }

    @Composable
    override fun GetErrorView() {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(30.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(stringResource(R.string.no_quests_found), style = SolarusTypography.headlineLarge)
            Spacer(modifier = Modifier.height(25.dp))
            Button(onClick = {
                FolderPreferenceFragment.newInstance().show(
                    requireActivity().supportFragmentManager,
                    FolderPreferenceFragment.TAG
                )
            }) {
                Text(
                    stringResource(R.string.change_quest_directory),
                    modifier = Modifier.padding(Constants.BUTTON_TEXT_PADDING.dp)
                )
            }
        }
    }
}