package org.solarus_games.solarus.quest

import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.os.FileUtils
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.solarus_games.solarus.EngineActivity
import org.solarus_games.solarus.MainActivity
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.recent.RecentlyPlayed
import org.solarus_games.solarus.recent.RecentlyPlayedDao
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import java.util.regex.Pattern
import java.util.zip.ZipFile
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class QuestManager @Inject constructor(
    private val application: Application,
    private val logger: Logger,
    private val preferencesManager: PreferencesManager,
    private val rpDao: RecentlyPlayedDao
) : ViewModel() {

    private val remoteFileDir = application.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)

    var currentQuest = MutableStateFlow<Quest?>(null)
        private set

    var quests = MutableStateFlow<List<Quest>>(emptyList())
        private set

    private var listeners = mutableListOf<QuestManagerListener>()

    var screenCapture = MutableStateFlow<Bitmap?>(null)
        private set

    init {
        preferencesManager.getQuestDirFlow().onEach { localDir ->
            localDir?.let {
                scanForSolarusQuests(it)
            }
        }.launchIn(viewModelScope)
    }

    suspend fun refreshQuests() {
        preferencesManager.getQuestDir()?.let {
            scanForSolarusQuests(it)
        }
    }


    fun addListener(listener: QuestManagerListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: QuestManagerListener) {
        listeners.remove(listener)
    }

    fun setScreenCapture(screen: Bitmap?) {
        viewModelScope.launch {
            screenCapture.emit(screen)
        }
    }

    fun setCurrentQuest(quest: Quest?) {
        viewModelScope.launch(Dispatchers.IO) {
            currentQuest.emit(quest)
            quest?.let {
                rpDao.insert(RecentlyPlayed(it.path))
            }
        }
    }

    private fun finishCurrentEngine() {
        viewModelScope.launch {
            currentQuest.emit(null)
            listeners.forEach { it.exit() }
        }
    }

    fun onQuestQuitRequest() {
        finishCurrentEngine()
    }

    fun onQuestDetailClick(context: Context, quest: Quest) {
        QuestDetailActivity.startActivity(context, quest)
    }

    fun onQuestResumeRequest(quest: Quest) {
        questResumeRequest(application, quest)
    }

    private fun questLaunchIntent(context: Context, quest: Quest): Intent {
        val intent = Intent(context, EngineActivity::class.java)
        intent.putExtra(EngineActivity.ARG_PATH, quest.path)
        return intent
    }

    fun launchQuest(context: Context, quest: Quest) {
        val intent = questLaunchIntent(context, quest)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (quest == currentQuest.value) {
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        }
        context.startActivity(intent)
    }

    fun shortcutIntent(context: Context, quest: Quest): Intent {
        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra(EngineActivity.ARG_PATH, quest.path)
        return intent
    }

    /**
     * Launch a quest but take care of closing any already existing
     *
     * @param quest
     */
    fun questLaunchRequest(context: Context, quest: Quest) {
        if (mustExitBeforeLaunch(quest)) {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle("Another quest is running")
            alertDialog.setMessage("Quit current quest?")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEGATIVE, "OK"
            ) { dialog, _ ->
                finishCurrentEngine()
                launchQuest(context, quest)
                dialog.dismiss()
            }
            alertDialog.setButton(
                AlertDialog.BUTTON_POSITIVE, "Cancel"
            ) { _, _ ->
                //Do nothing...
            }
            alertDialog.show()
        } else {
            launchQuest(context, quest)
        }
    }

    /**
     * request to resume an already running quest
     *
     * @param context
     * @param quest
     */
    private fun questResumeRequest(context: Context, quest: Quest) {
        val intent = Intent(context, EngineActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (quest == currentQuest.value) {
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        }
        intent.putExtra(EngineActivity.ARG_PATH, quest.path)
        context.startActivity(intent)
    }

    fun nowPlaying(quest: Quest?) = currentQuest.value == quest

    private fun mustExitBeforeLaunch(quest: Quest?): Boolean {
        return currentQuest.value != null && currentQuest.value != quest
    }

    private fun getFieldOrEmpty(p: Pattern, data: String): String {
        val m = p.matcher(data)
        return if (m.find()) {
            m.group(1)!!
        } else ""
    }

    private suspend fun scanForSolarusQuests(localDir: String) {
        val local = scanForSolarusQuest(File(localDir), 1)
        val remote = remoteFileDir?.let { scanForSolarusQuest(it, 1, true) } ?: emptyList()
        quests.emit(remote.plus(local))
    }

    private fun scanForSolarusQuest(dir: File, depth: Int, isRemote: Boolean = false): List<Quest> {
        logger.d(TAG, "scanForSolarusQuest file: ${dir.name}")
        val list = ArrayList<Quest>()
        if (dir.exists() && dir.isDirectory) {
            val files = dir.listFiles()
            if (files != null) {
                for (f in files) {
                    if (isASolarusQuest(f)) {
                        fromPath(f.absolutePath, isRemote)?.let { list.add(it) }
                    } else if (f.isDirectory && depth >= 0) {
                        list.addAll(scanForSolarusQuest(f, depth - 1, isRemote))
                    }
                }
            }
        }
        return list
    }

    fun getQuests(paths: List<String>): List<Quest> {
        val list = ArrayList<Quest>()
        paths.forEach { path ->
            val f = File(path)
            if (isASolarusQuest(f)) {
                fromPath(f.absolutePath)?.let { list.add(it) }
            }
        }
        return list
    }

    private fun fromQuestDatAndImages(path: String, questDatContent: String, logo: Bitmap, icon: Bitmap, remoteId: String?): Quest {
        val longDescription = getFieldOrEmpty(longDescriptionPattern, questDatContent)
        val shortDescription = getFieldOrEmpty(shortDescriptionPattern, questDatContent)
        val title = getFieldOrEmpty(titlePattern, questDatContent)
        val version = getFieldOrEmpty(versionPattern, questDatContent)
        val format = getFieldOrEmpty(formatPattern, questDatContent)
        val author = getFieldOrEmpty(authorPattern, questDatContent)
        val releaseDate = getFieldOrEmpty(releaseDatePattern, questDatContent)
        val website = getFieldOrEmpty(websitePattern, questDatContent)
        return Quest(path, title, shortDescription, longDescription, version, format, author, releaseDate, website, logo, icon, remoteId)
    }

    @Throws(IOException::class)
    private fun iStreamToBytes(stream: InputStream, size: Int): ByteArray {
        val buffer = ByteArray(size)
        stream.read(buffer)
        return buffer
    }

    @Throws(IOException::class)
    private fun iStreamToString(stream: InputStream, size: Int): String {
        return String(iStreamToBytes(stream, size), Charset.forName("UTF8"))
    }


    @Throws(IOException::class)
    fun getLogoFromZip(zipFile: ZipFile): Bitmap {
        val logoEntry = zipFile.getEntry(logoFileName)
            ?: return BitmapFactory.decodeResource(application.resources, R.drawable.no_logo)
        return BitmapFactory.decodeStream(zipFile.getInputStream(logoEntry))
    }

    @Throws(IOException::class)
    fun getLogoFromFolder(folder: String): Bitmap {
        val logoFile = File(folder + logoFileName)
        return if (!logoFile.exists()) {
            BitmapFactory.decodeResource(application.resources, R.drawable.no_logo)
        } else BitmapFactory.decodeStream(FileInputStream(logoFile))
    }

    @Throws(IOException::class)
    fun getIconFromFolder(folder: String): Bitmap {
        for (fileName in iconFileNames) {
            val file = File(folder + fileName)
            if (file.exists()) {
                return BitmapFactory.decodeStream(FileInputStream(file))
            }
        }
        return BitmapFactory.decodeResource(application.resources, R.drawable.default_icon)
    }

    @Throws(IOException::class)
    fun getIconFromZip(zipFile: ZipFile): Bitmap {
        for (fileName in iconFileNames) {
            val fileEntry = zipFile.getEntry(fileName)
            if (fileEntry != null) {
                return BitmapFactory.decodeStream(zipFile.getInputStream(fileEntry))
            }
        }
        return BitmapFactory.decodeResource(application.resources, R.drawable.default_icon)
    }

    private fun isASolarusQuest(fileToTest: File): Boolean {
        // A Solarus games is :
        // - a zip file named game_file.solarus
        // - a folder containing ./data/quest.dat
        if (!fileToTest.isDirectory && fileToTest.absolutePath.endsWith(QUEST_EXTENSION)) {
            return true
        }
        val dataFolder = File(fileToTest, "data")
        if (dataFolder.exists()) {
            if (dataFolder.isDirectory) {
                val questFile = File(dataFolder, "quest.dat")
                return questFile.exists()
            }
        }
        return false
    }

    private fun fromZipFile(path: String, remoteId: String? = null): Quest? {
        return try {
            val zipFile = ZipFile(path)
            val questDatEntry = zipFile.getEntry("quest.dat")
            if (questDatEntry == null) {
                Log.d(TAG, "no quest.dat found!")
                return null
            }
            val questDatIStream = zipFile.getInputStream(questDatEntry)
            val questDatContent = iStreamToString(questDatIStream, questDatEntry.size.toInt())
            fromQuestDatAndImages(
                path,
                questDatContent,
                getLogoFromZip(zipFile),
                getIconFromZip(zipFile),
                remoteId
            )
        } catch (e: IOException) {
            null
        }
    }

    private fun fromFolder(path: String, remoteId: String? = null): Quest? {
        return try {
            val dataFolder = "$path/data/"
            val questDatFile = File(dataFolder + "quest.dat")
            if (!questDatFile.exists()) {
                return null
            }
            val questDatIStream: InputStream = FileInputStream(questDatFile)
            val questDatContent = iStreamToString(questDatIStream, questDatFile.length().toInt())
            fromQuestDatAndImages(
                path,
                questDatContent,
                getLogoFromFolder(dataFolder),
                getIconFromFolder(dataFolder),
                remoteId
            )
        } catch (e: IOException) {
            null
        }
    }

    fun fromPath(path: String, isRemote: Boolean = false): Quest? {
        val file = File(path)
        val remoteId = if (isRemote) file.absolutePath.substringAfterLast("/").substringBefore(QUEST_EXTENSION) else null
        return if (!file.exists()) {
            null
        } else if (file.isDirectory) {
            fromFolder(path, remoteId)
        } else {
            fromZipFile(path, remoteId)
        }
    }

    fun getQuestForGameId(id: String): Quest? {
        if (isRemoteQuestDownloaded(id)) {
            return getRemoteQuest(id)
        }
        return null
    }

    fun isRemoteQuestDownloaded(id: String): Boolean {
        val file = getRemoteQuestFile(id)
        return file.exists()
    }

    fun saveRemoteQuest(filePath: String): Boolean {
        val importFile = File(filePath)
        val destinationFile = File(application.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), importFile.name)
        if (!destinationFile.exists()) destinationFile.createNewFile()

        FileUtils.copy(FileInputStream(filePath), FileOutputStream(destinationFile, false))
        return true
    }

    suspend fun saveRemoteQuest(body: ResponseBody?, id: String): Boolean = withContext(Dispatchers.IO) {
        if (body == null) return@withContext false
        var input: InputStream? = null
        try {
            input = body.byteStream()
            val file = getRemoteQuestFile(id)
            if (file.exists()) file.delete()
            file.createNewFile()
            val fos = FileOutputStream(file)
            fos.use { output ->
                val buffer = ByteArray(4 * 1024) // or other buffer size
                var read: Int
                while (input.read(buffer).also { read = it } != -1) {
                    output.write(buffer, 0, read)
                }
                output.flush()
            }
            refreshQuests()
            return@withContext true
        } catch (e: Exception) {
            Log.e("saveFile", e.toString())
        } finally {
            input?.close()
        }
        return@withContext false
    }

    private fun getRemoteQuest(id: String) = fromPath(getRemoteQuestFile(id).absolutePath, true)

    fun getRemoteQuestFile(id: String) = File(remoteFileDir, "$id${QUEST_EXTENSION}")

    fun deleteRemoteQuestById(id: String) {
        getRemoteQuestFile(id).let {
            if (it.exists()) it.delete()
        }
        viewModelScope.launch {
            refreshQuests()
        }
    }

    fun deleteQuestByPath(path: String) {
        File(path).let {
            if (it.exists()) it.delete()
        }
        viewModelScope.launch {
            refreshQuests()
        }
    }

    fun deleteAllRemoteQuests() {
        viewModelScope.launch {
            getRemoteQuestFiles().forEach { questPath -> deleteQuestByPath(questPath) }
            refreshQuests()
        }
    }

    private fun getRemoteQuestFiles(): List<String> {
        return remoteFileDir?.listFiles { directory, filename ->
            directory.length() > 0 && filename.endsWith(QUEST_EXTENSION)
        }?.map { file -> file.absolutePath } ?: emptyList()
    }


    companion object {
        const val TAG = "QuestManager"
        private const val QUEST_EXTENSION = ".solarus"

        private fun makeFieldPattern(field: String): Pattern {
            return Pattern.compile("$field = \"([^\"]*)")
        }

        val longDescriptionPattern: Pattern = Pattern.compile("long_description = \\[\\[\n([^\\]]*)")
        val shortDescriptionPattern = makeFieldPattern("short_description")
        val titlePattern = makeFieldPattern("title")
        val versionPattern = makeFieldPattern("quest_version")
        val authorPattern = makeFieldPattern("author")
        val formatPattern = makeFieldPattern("solarus_version")
        val releaseDatePattern = makeFieldPattern("release_date")
        val websitePattern = makeFieldPattern("website")
        const val logoFileName = "logos/logo.png"
        val iconFileNames = arrayOf(
            "logos/icon_128.png",
            "logos/icon_64.png",
            "logos/icon_48.png",
            "logos/icon_32.png",
            "logos/icon_24.png",
            "logos/icon_16.png"
        )
    }
}

interface QuestManagerListener {
    fun exit()
}