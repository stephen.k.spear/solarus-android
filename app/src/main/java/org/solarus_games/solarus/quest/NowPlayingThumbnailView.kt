package org.solarus_games.solarus.quest

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SolarusTypography

@Composable
fun NowPlayingThumbnailView(isLarge: Boolean = false) {
    Row(
        Modifier
            .background(Secondary)
            .fillMaxWidth()
            .padding(5.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(id = R.drawable.play), contentDescription = "Currently Playing",
            modifier = Modifier
                .padding(horizontal = 5.dp)
                .size(if (isLarge) 16.dp else 12.dp),
            colorFilter = ColorFilter.tint(Color.White)
        )
        Text(
            text = stringResource(id = if (isLarge) R.string.currently_playing else R.string.playing),
            style = if (isLarge) SolarusTypography.bodyLarge.copy(color = Color.White, fontWeight = FontWeight.W600)
            else SolarusTypography.labelSmall.copy(color = Color.White, fontWeight = FontWeight.W600)
        )
    }
}