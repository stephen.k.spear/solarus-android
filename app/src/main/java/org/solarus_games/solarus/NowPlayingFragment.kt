package org.solarus_games.solarus

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.common.ComposeFragment
import org.solarus_games.solarus.quest.NowPlayingView
import org.solarus_games.solarus.quest.QuestManager
import javax.inject.Inject


@AndroidEntryPoint
abstract class NowPlayingFragment : ComposeFragment() {

    @Inject
    lateinit var questManager: QuestManager

    @Composable
    override fun GetNowPlaying() {
        val currentQuest by questManager.currentQuest.collectAsState(null)
        if (currentQuest != null) {
            NowPlayingView(modifier = Modifier, manager = questManager)
        }
    }
}