package org.solarus_games.solarus

import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.common.SearchBar
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryTextLight

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun QuestHeader(
    @StringRes title: Int,
    preferencesManager: PreferencesManager,
    search: String,
    updateSearch: (search: String) -> Unit
) {
    var showSearch by remember { mutableStateOf(false) }
    Column(
        Modifier
            .fillMaxWidth()
            .background(Primary)
    ) {

        CenterAlignedTopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(containerColor = Color.Transparent),
            title = {
                Text(
                    stringResource(id = title),
                    style = MaterialTheme.typography.headlineLarge,
                    modifier = Modifier.padding(vertical = 10.dp),
                    textAlign = TextAlign.Center,
                    color = SecondaryLight
                )
            },
            navigationIcon = {
                if (showSearch) {
                    Icon(
                        painter = painterResource(id = R.drawable.empty),
                        contentDescription = "Empty",
                    )
                } else {
                    IconButton(onClick = {
                        showSearch = true
                    }) {
                        Icon(
                            painter = painterResource(id = R.drawable.search),
                            contentDescription = "Search",
                            tint = SecondaryTextLight
                        )
                    }
                }
            },
            actions = {
                Box {
                    val useGrid by preferencesManager.useGridViewFlow().collectAsState(initial = false)
                    val sortDescending by preferencesManager.sortDescendingFlow().collectAsState(initial = true)
                    var menuExpanded by remember { mutableStateOf(false) }
                    IconButton(onClick = { menuExpanded = !menuExpanded }) {
                        Icon(
                            painter = painterResource(id = R.drawable.menu_dots),
                            contentDescription = "Menu Dots",
                            tint = SecondaryTextLight
                        )
                    }
                    DropdownMenu(
                        expanded = menuExpanded,
                        onDismissRequest = { menuExpanded = false },
                        modifier = Modifier.background(PrimaryLight)
                    ) {
                        DropdownMenuItem(
                            text = {
                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(stringResource(id = if (useGrid) R.string.column_view else R.string.grid_wiew))
                                    Icon(
                                        painter = painterResource(id = R.drawable.play),
                                        contentDescription = "View",
                                        tint = SecondaryTextLight,
                                        modifier = Modifier.size(10.dp)
                                    )
                                }
                            },
                            onClick = {
                                menuExpanded = false
                                preferencesManager.setUseGridView(!useGrid)
                            },
                        )
                        DropdownMenuItem(
                            text = {
                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(stringResource(id = if (sortDescending) R.string.sort_ascending else R.string.sort_descending))
                                    Icon(
                                        painter = painterResource(id = R.drawable.play),
                                        contentDescription = "View",
                                        tint = SecondaryTextLight,
                                        modifier = Modifier.size(10.dp)
                                    )
                                }
                            },
                            onClick = {
                                menuExpanded = false
                                preferencesManager.setSortDescending(!sortDescending)
                            },
                        )
                    }
                }
            }
        )

        AnimatedVisibility(visible = showSearch) {
            SearchBar(modifier = Modifier.fillMaxWidth(), search, updateSearch) {
                showSearch = false
            }
        }
    }
}