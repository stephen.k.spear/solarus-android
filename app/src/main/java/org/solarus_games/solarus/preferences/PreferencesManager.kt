package org.solarus_games.solarus.preferences

import android.app.Application
import android.os.Environment
import androidx.preference.PreferenceManager
import org.solarus_games.solarus.utils.getBooleanFlow
import org.solarus_games.solarus.utils.getIntFlow
import org.solarus_games.solarus.utils.getStringFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesManager @Inject constructor(application: Application) {

    private val prefs = PreferenceManager.getDefaultSharedPreferences(application)

    fun getVibrationDuration() = VIBRATION_DURATION


    fun getQuestDir() = prefs.getString("quest_folder", "${Environment.getExternalStorageDirectory()}/Quests/")

    fun getQuestDirFlow() = prefs.getStringFlow("quest_folder", "${Environment.getExternalStorageDirectory()}/Quests/")
    fun setQuestDir(dir: String?) = prefs.edit().putString("quest_folder", dir).apply()

    fun isGamepadEnabled() = prefs.getBoolean("virtual_gamepad", true)
    fun isGamepadEnabledFlow() = prefs.getBooleanFlow("virtual_gamepad", true)
    fun setGamepadEnabled(enabled: Boolean) = prefs.edit().putBoolean("virtual_gamepad", enabled).apply()

    fun isVibrationEnabled() = prefs.getBoolean("virtual_gamepad_vibrate", true)
    fun isVibrationEnabledFlow() = prefs.getBooleanFlow("virtual_gamepad_vibrate", true)
    fun setVibrationEnabled(enabled: Boolean) = prefs.edit().putBoolean("virtual_gamepad_vibrate", enabled).apply()

    fun isVibrateDpad() = prefs.getBoolean("virtual_gamepad_dpad_vibrate", true)
    fun isVibrateDpadFlow() = prefs.getBooleanFlow("virtual_gamepad_dpad_vibrate", true)
    fun setVibrateDpad(enabled: Boolean) = prefs.edit().putBoolean("virtual_gamepad_dpad_vibrate", enabled).apply()

    fun isAudioEnabled() = prefs.getBoolean("audio", true)
    fun isAudioEnabledFlow() = prefs.getBooleanFlow("audio", true)

    fun setAudioEnabled(enabled: Boolean) = prefs.edit().putBoolean("audio", enabled).apply()

    fun isIgnoreLayoutSizePreferencesEnabled() = prefs.getBoolean("virtual_gamepad_ignore_size", false)
    fun isIgnoreLayoutSizePreferencesEnabledFlow() = prefs.getBooleanFlow("virtual_gamepad_ignore_size", false)
    fun setIgnoreLayoutSizePreferencesEnabled(enabled: Boolean) =
        prefs.edit().putBoolean("virtual_gamepad_ignore_size", enabled).apply()

    fun getLayoutTransparency() = prefs.getInt("virtual_gamepad_opacity", 128)
    fun getLayoutTransparencyFlow() = prefs.getIntFlow("virtual_gamepad_opacity", 128)

    fun setLayoutTransparency(value: Int) = prefs.edit().putInt("virtual_gamepad_opacity", value).apply()

    fun getLayoutSize() = prefs.getInt("virtual_gamepad_size", 100)
    fun getLayoutSizeFlow() = prefs.getIntFlow("virtual_gamepad_size", 100)

    fun setLayoutSize(value: Int) = prefs.edit().putInt("virtual_gamepad_size", value).apply()

    fun isForcedLandscape() = prefs.getBoolean("force_landscape", false)
    fun isForcedLandscapeFlow() = prefs.getBooleanFlow("force_landscape", false)

    fun setForcedLandscape(enabled: Boolean) = prefs.edit().putBoolean("force_landscape", enabled).apply()

    fun useGridViewFlow() = prefs.getBooleanFlow("use_grid", false)

    fun setUseGridView(gridView: Boolean) = prefs.edit().putBoolean("use_grid", gridView).apply()

    fun sortDescendingFlow() = prefs.getBooleanFlow("sort_descending", false)

    fun setSortDescending(sortDescending: Boolean) = prefs.edit().putBoolean("sort_descending", sortDescending).apply()

    fun getString(key: String) = prefs.getString(key, null)

    fun getStringFlow(key: String) = prefs.getStringFlow(key, null)

    fun setString(key: String, value: String?) = prefs.edit().putString(key, value).apply()

    fun resetSettings() {
        val editor = prefs.edit()
        prefs.all.forEach {
            editor.putString(it.key, null)
        }
        editor.apply()
    }


    companion object {
        private const val VIBRATION_DURATION: Long = 20
    }
}