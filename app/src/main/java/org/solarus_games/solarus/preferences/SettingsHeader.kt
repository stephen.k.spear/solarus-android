package org.solarus_games.solarus.preferences

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryTextLight

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsHeader(@StringRes title: Int, preferencesManager: PreferencesManager, onBackPressed: () -> Unit) {
    CenterAlignedTopAppBar(
        colors = TopAppBarDefaults.topAppBarColors(containerColor = Primary),
        title = {
            Text(
                stringResource(id = title),
                style = MaterialTheme.typography.headlineLarge,
                modifier = Modifier.padding(vertical = 10.dp),
                textAlign = TextAlign.Center,
                color = SecondaryLight
            )
        },
        navigationIcon = {
            IconButton(onClick = { onBackPressed() }) {
                Icon(
                    painter = painterResource(id = R.drawable.arrow_left),
                    contentDescription = "Back",
                    tint = SecondaryTextLight
                )
            }
        },
        actions = {
            Box {
                var menuExpanded by remember { mutableStateOf(false) }
                IconButton(onClick = { menuExpanded = !menuExpanded }) {
                    Icon(
                        painter = painterResource(id = R.drawable.menu_dots),
                        contentDescription = "Menu Dots",
                        tint = SecondaryTextLight
                    )
                }
                DropdownMenu(
                    expanded = menuExpanded,
                    onDismissRequest = { menuExpanded = false },
                    modifier = Modifier.background(PrimaryLight)
                ) {
                    DropdownMenuItem(
                        text = {
                            Text(stringResource(R.string.restore_default_settings))
                        },
                        onClick = {
                            menuExpanded = false
                            preferencesManager.resetSettings()
                        },
                    )
                }
            }
        },
    )
}