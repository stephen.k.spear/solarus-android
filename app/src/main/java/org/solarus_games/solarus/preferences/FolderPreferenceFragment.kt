package org.solarus_games.solarus.preferences

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import com.codekidlabs.storagechooser.StorageChooser
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.ComposeDialog
import org.solarus_games.solarus.common.Constants.BUTTON_TEXT_PADDING
import org.solarus_games.solarus.theme.SolarusTypography
import javax.inject.Inject

@AndroidEntryPoint
class FolderPreferenceFragment : ComposeDialog() {

    @Inject
    lateinit var prefsManager: PreferencesManager

    @Composable
    override fun GetContent() {
        val keyboardController = LocalSoftwareKeyboardController.current
        var questDir by remember { mutableStateOf(prefsManager.getQuestDir().orEmpty()) }
        Column(
            Modifier
                .fillMaxWidth()
                .padding(25.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(15.dp)
        ) {

            Text(stringResource(id = R.string.enter_search_folder_path), style = SolarusTypography.headlineMedium)

            TextField(
                modifier = Modifier.fillMaxWidth(),
                maxLines = 1,
                value = questDir,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done,
                ),
                keyboardActions = KeyboardActions(
                    onDone = { keyboardController?.hide() }
                ),
                onValueChange = {
                    questDir = it
                },
            )

            Column(
                Modifier
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                Button(
                    onClick = {
                        prefsManager.setQuestDir(questDir)
                        dismissAllowingStateLoss()
                    }, modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        stringResource(id = R.string.ok),
                        modifier = Modifier.padding(BUTTON_TEXT_PADDING.dp)
                    )
                }
                Button(
                    onClick = {
                        val chooser = StorageChooser.Builder()
                            .withActivity(activity)
                            .allowCustomPath(true)
                            .withFragmentManager(requireActivity().fragmentManager)
                            .setType(StorageChooser.DIRECTORY_CHOOSER)
                            .build()

                        chooser.setOnSelectListener { path ->
                            questDir = path
                            prefsManager.setQuestDir(path)
                            dismissAllowingStateLoss()
                        }
                        chooser.show()
                    }, modifier = Modifier.fillMaxWidth()
                ) {
                    Text(stringResource(id = R.string.browse),
                        modifier = Modifier.padding(BUTTON_TEXT_PADDING.dp))
                }
            }
        }
    }

    companion object {
        const val TAG = "FolderPrefs"

        fun newInstance(): FolderPreferenceFragment {
            return FolderPreferenceFragment()
        }
    }
}