package org.solarus_games.solarus.preferences

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.SETTING_SPACING
import org.solarus_games.solarus.common.PreferenceDivider
import org.solarus_games.solarus.common.SliderPreference
import org.solarus_games.solarus.common.SwitchPreference

@Composable
fun GamepadView(
    prefsManager: PreferencesManager,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 20.dp),
        verticalArrangement = Arrangement.spacedBy(SETTING_SPACING.dp),
        contentPadding = PaddingValues(vertical = 20.dp)
    ) {
        item {
            PreferenceDivider(title = R.string.preferences_cat_general)
        }

        item {
            val value by prefsManager.isGamepadEnabledFlow().collectAsState(initial = true)
            SwitchPreference(
                title = R.string.preferences_virtual_gamepad,
                message = R.string.preferences_virtual_gamepad_sum,
                value
            ) {
                prefsManager.setGamepadEnabled(it)
            }
        }

        item {
            val value by prefsManager.isIgnoreLayoutSizePreferencesEnabledFlow().collectAsState(initial = true)
            SwitchPreference(
                title = R.string.preferences_ignore_size,
                message = R.string.preferences_ignore_size_sum,
                value
            ) {
                prefsManager.setIgnoreLayoutSizePreferencesEnabled(it)
            }
        }

        item {
            PreferenceDivider(title = R.string.appearance)
        }

        item {
            val value by prefsManager.getLayoutTransparencyFlow().collectAsState(initial = 0)
            SliderPreference(title = R.string.preferences_opacity, value) {
                prefsManager.setLayoutTransparency(it)
            }
        }

        item {
            val value by prefsManager.getLayoutSizeFlow().collectAsState(initial = 0)
            SliderPreference(title = R.string.preferences_layout_size, value) {
                prefsManager.setLayoutSize(it)
            }
        }

        item {
            val value by prefsManager.isVibrationEnabledFlow().collectAsState(initial = true)
            SwitchPreference(
                title = R.string.preferences_vibrate,
                message = R.string.preferences_vibrate_sum,
                value
            ) {
                prefsManager.setVibrationEnabled(it)
            }
        }

        item {
            val value by prefsManager.isVibrateDpadFlow().collectAsState(initial = true)
            SwitchPreference(
                title = R.string.preferences_dpad_vibrate,
                message = R.string.preferences_dpad_vibrate_sum,
                value
            ) {
                prefsManager.setVibrateDpad(it)
            }
        }

        item {
            JoypadPreference(modifier = Modifier.fillMaxWidth())
        }
    }

}