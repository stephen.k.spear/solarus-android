package org.solarus_games.solarus.preferences

import android.content.Context
import android.content.Intent
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.solarus_games.solarus.R
import org.solarus_games.solarus.joypad.ButtonMappingActivity
import org.solarus_games.solarus.joypad.ButtonMappingManager
import org.solarus_games.solarus.joypad.ButtonMappingManager.InputLayout
import javax.inject.Inject

@HiltViewModel
class JoypadPreferenceViewModel @Inject constructor(
    private val buttonMappingManager: ButtonMappingManager,
) : ViewModel() {

    var inputs = MutableStateFlow<List<InputLayout>>(emptyList())

    var defaultId = MutableStateFlow(0)

    init {
        getInputs()
    }

    private fun getInputs() {
        viewModelScope.launch {
            inputs.emit(buttonMappingManager.getLayoutList().toList())
            defaultId.emit(buttonMappingManager.defaultLayoutId)
        }
    }

    fun editInputLayout(context: Context, gameLayout: InputLayout) {
        val intent = Intent(context, ButtonMappingActivity::class.java)
        intent.putExtra(ButtonMappingActivity.TAG_ID, gameLayout.id)
        context.startActivity(intent)
    }

    fun addNewInputLayout(newInputLayout: InputLayout) {
        viewModelScope.launch {
            inputs.emit(inputs.value.plus(newInputLayout))
        }
        buttonMappingManager.add(newInputLayout)
        buttonMappingManager.save()
    }

    fun setAsDefaultLayout(id: Int) {
        buttonMappingManager.setDefaultLayout(id)
        buttonMappingManager.save()
        viewModelScope.launch {
            defaultId.emit(buttonMappingManager.defaultLayoutId)
        }
    }

    fun deleteInputLayout(context: Context, input: InputLayout) {
        buttonMappingManager.delete(context, input)
        getInputs()
    }

    fun editInputLayoutName(context: Context, inputLayout: InputLayout) {
        // The editText field
        val input = EditText(context)
        input.setText(inputLayout.name)

        // The dialog box
        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.edit_name).setView(input)
            .setPositiveButton(R.string.ok) { _, _ ->
                val text = input.text.toString()
                if (text.isNotEmpty()) {
                    inputLayout.name = text
                }
                buttonMappingManager.save()
                getInputs()
            }.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
        builder.show()
    }
}