package org.solarus_games.solarus.preferences

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.SETTING_SPACING
import org.solarus_games.solarus.common.Preference
import org.solarus_games.solarus.common.PreferenceDivider
import org.solarus_games.solarus.common.SwitchPreference
import org.solarus_games.solarus.quest.QuestManagerViewModel
import org.solarus_games.solarus.utils.getSupportFragmentManager

@Composable
fun QuestsView(
    prefsManager: PreferencesManager,
    questManagerViewModel: QuestManagerViewModel = hiltViewModel(),
) {
    val context = LocalContext.current
    val questDir by prefsManager.getQuestDirFlow().collectAsState(initial = stringResource(R.string.preferences_quests_path_sum))
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 20.dp),
        verticalArrangement = Arrangement.spacedBy(SETTING_SPACING.dp),
        contentPadding = PaddingValues(vertical = 20.dp)
    ) {
        item {
            PreferenceDivider(title = R.string.location)
        }

        item {
            Preference(title = R.string.preferences_quests_path, message = questDir) {
                context.getSupportFragmentManager()?.let {
                    FolderPreferenceFragment.newInstance().show(
                        it, FolderPreferenceFragment.TAG
                    )
                }
            }
        }

        item {
            PreferenceDivider(title = R.string.updates)
        }

        item {
            SwitchPreference(
                title = R.string.preferences_auto_updates,
                message = R.string.preferences_auto_updates_sum,
                value = false,
                enabled = false
            ) {
                //TODO Implementation
            }
        }

        item {
            Preference(
                title = R.string.preferences_auto_update_freq,
                message = stringResource(id = R.string.preferences_auto_update_freq_sum),
                enabled = false
            )
        }

        item {
            PreferenceDivider(title = R.string.data)
        }

        item {
            Preference(
                title = R.string.delete_download_quests,
                message = stringResource(id = R.string.delete_download_quests_sum)
            ) {
                questManagerViewModel.deleteAllRemoteQuests()
            }
        }
    }
}