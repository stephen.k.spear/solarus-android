package org.solarus_games.solarus.shop

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.solarus_games.solarus.common.Resource
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.quest.Quest
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.remote.use_cases.DownloadGameUseCase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GameDetailActivityViewModel @Inject constructor(
    val preferencesManager: PreferencesManager,
    val questManager: QuestManager,
    private val downloadGameUseCase: DownloadGameUseCase
) : ViewModel() {

    var localQuest = MutableStateFlow<Quest?>(null)
        private set

    fun updateGameInfo(game: Game) {
        viewModelScope.launch {
            localQuest.emit(questManager.getQuestForGameId(game.id))
        }
    }

    fun downloadAndLaunchGame(context: Context, game: Game, callback: (result: Boolean) -> Unit) {
        viewModelScope.launch {
            if (questManager.isRemoteQuestDownloaded(game.id)) {
                questManager.fromPath(questManager.getRemoteQuestFile(game.id).absolutePath)?.let {
                    questManager.launchQuest(context, it)
                    callback(true)
                }
            } else {
                downloadGame(context, game) { result ->
                    if (result) {
                        questManager.getQuestForGameId(game.id)?.let { quest ->
                            questManager.launchQuest(context, quest)
                        }
                    }
                    callback(result)
                }
            }
        }
    }

    fun deleteGame(game: Game) {
        questManager.deleteRemoteQuestById(game.id)
        updateGameInfo(game)
    }

    fun downloadGame(context: Context, game: Game, callback: ((result: Boolean) -> Unit)? = null) {
        downloadGameUseCase(context, game).onEach { result ->
            when (result) {
                is Resource.Loading -> {
                    // do nothing dialog handled by use case
                }

                is Resource.Success -> {
                    updateGameInfo(game)
                    callback?.invoke(questManager.isRemoteQuestDownloaded(game.id))
                }

                is Resource.Error -> {
                    callback?.invoke(false)
                    Toast.makeText(
                        context,
                        "Error Downloading Game",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }.launchIn(viewModelScope)
    }
}