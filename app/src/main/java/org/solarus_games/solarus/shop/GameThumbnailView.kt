package org.solarus_games.solarus.shop

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.SubcomposeAsyncImage
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.SOLARUS_BASE_URL
import org.solarus_games.solarus.quest.NowPlayingThumbnailView
import org.solarus_games.solarus.quest.QuestManagerViewModel
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.utils.shimmerEffect

@Composable
fun GameThumbnailView(
    modifier: Modifier,
    game: Game,
    isLarge: Boolean = false,
    questManagerViewModel: QuestManagerViewModel = hiltViewModel(),
    onGameClick: () -> Unit
) {
    val quest = remember(game) { questManagerViewModel.getQuestForGame(game) }
    val currentQuest by questManagerViewModel.currentQuest().collectAsState(null)
    with(game) {
        Card(
            onClick = {
                onGameClick()
            },
            shape = RectangleShape,
            elevation = CardDefaults.cardElevation(
                defaultElevation = 15.dp,
                pressedElevation = 0.dp,
                hoveredElevation = 8.dp
            ),
            modifier = modifier
        ) {
            Box(contentAlignment = Alignment.BottomCenter) {
                if (thumbnail.isNotEmpty()) {
                    SubcomposeAsyncImage(
                        model = SOLARUS_BASE_URL.plus(thumbnail),
                        loading = {
                            Box(
                                Modifier
                                    .background(Color.Blue)
                                    .fillMaxSize()
                                    .shimmerEffect()
                            )
                        },
                        contentDescription = title,
                        modifier = Modifier.fillMaxSize(),
                        contentScale = ContentScale.Crop
                    )
                } else {
                    Image(
                        painterResource(id = R.drawable.placeholder_thumbnail),
                        contentDescription = title,
                        contentScale = ContentScale.Fit,
                        modifier = Modifier.fillMaxSize(),
                    )
                }

                if (quest != null && quest.path == currentQuest?.path) {
                    NowPlayingThumbnailView(isLarge)
                }
            }
        }
    }
}