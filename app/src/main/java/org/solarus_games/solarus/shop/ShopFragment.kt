package org.solarus_games.solarus.shop

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.BaseQuestListFragment
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Center
import org.solarus_games.solarus.common.Constants
import org.solarus_games.solarus.common.Constants.COLUMN_PADDING
import org.solarus_games.solarus.common.Constants.LARGE_CARD_CORNERS
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.theme.SolarusTypography
import javax.inject.Inject

@AndroidEntryPoint
class ShopFragment : BaseQuestListFragment<Game>() {

    @Inject
    lateinit var viewModel: ShopViewModel

    override val getHeaderStringRes = R.string.shop

    override fun getPreferencesManager() = viewModel.preferencesManager

    override fun handleRefresh() = viewModel.getGames(false)

    override fun shouldFilter(search: String, item: Game) = item.title.contains(search, true)

    @Composable
    override fun GetContent() {
        val state by viewModel.state.collectAsState(ShopState(loading = true))
        with(state) {
            when {
                games.isNotEmpty() -> QuestListView(games)
                loading -> {
                    Center(Modifier.fillMaxSize()) {
                        CircularProgressIndicator()
                    }
                }

                else -> {
                    Center(Modifier.fillMaxSize()) {
                        Text("Error loading shop")
                    }
                }
            }
        }
    }

    @Composable
    override fun GetItemView(item: Game) {
        val context = LocalContext.current
        val useGrid by viewModel.preferencesManager.useGridViewFlow().collectAsState(initial = false)
        if (useGrid) {
            GameThumbnailView(
                modifier = Modifier
                    .padding(horizontal = COLUMN_PADDING.dp)
                    .clip(RoundedCornerShape(LARGE_CARD_CORNERS.dp))
                    .fillMaxWidth()
                    .height(200.dp), game = item, true
            ) {
                GameDetailActivity.startActivity(context, item)
            }
        } else {
            GameColumnView(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp), game = item
            ) {
                GameDetailActivity.startActivity(context, item)
            }
        }
    }

    @Composable
    override fun GetErrorView() {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(30.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(stringResource(R.string.games_error), style = SolarusTypography.headlineLarge)
            Spacer(modifier = Modifier.height(25.dp))
            Button(onClick = {
                viewModel.getGames()
            }) {
                Text(
                    stringResource(R.string.try_again),
                    modifier = Modifier.padding(Constants.BUTTON_TEXT_PADDING.dp)
                )
            }
        }
    }
}
