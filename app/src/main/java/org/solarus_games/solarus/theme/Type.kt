package org.solarus_games.solarus.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import org.solarus_games.solarus.R

val ubuntu_bold = FontFamily(Font(R.font.ubuntu_bold))
val ubuntu_bold_italic = FontFamily(Font(R.font.ubuntu_bold_italic))
val ubuntu_light = FontFamily(Font(R.font.ubuntu_light))
val ubuntu_light_italic = FontFamily(Font(R.font.ubuntu_light_italic))
val ubuntu_medium = FontFamily(Font(R.font.ubuntu_medium))
val ubuntu_medium_italic = FontFamily(Font(R.font.ubuntu_medium_italic))
val ubuntu_regular = FontFamily(Font(R.font.ubuntu_regular))
val ubuntu_regular_italic = FontFamily(Font(R.font.ubuntu_regular_italic))
val ubuntu_titling = FontFamily(Font(R.font.ubuntu_titling))

val SolarusTypography = Typography(
    headlineLarge = TextStyle(
        fontFamily = ubuntu_titling,
        fontWeight = FontWeight.Bold,
        fontSize = 24.sp,
        color = PrimaryText
    ),
    headlineMedium = TextStyle(
        fontFamily = ubuntu_medium,
        fontWeight = FontWeight.Medium,
        fontSize = 22.sp,
        color = PrimaryText
    ),
    bodyLarge = TextStyle(
        fontFamily = ubuntu_medium,
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp,
        color = PrimaryText
    ),
    bodyMedium = TextStyle(
        fontFamily = ubuntu_regular,
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp,
        color = PrimaryText
    ),
    labelLarge = TextStyle(
        fontFamily = ubuntu_medium,
        fontWeight = FontWeight.Medium,
        fontSize = 14.sp,
        letterSpacing = 0.25.sp,
        color = PrimaryText
    ),
    labelSmall = TextStyle(
        fontFamily = ubuntu_regular,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp,
        color = PrimaryText
    )
)