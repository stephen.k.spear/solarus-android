package org.solarus_games.solarus.theme

import android.annotation.SuppressLint
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable

@SuppressLint("ConflictingOnColor")
private val DarkColorPalette = darkColorScheme(
    primary = Primary,
    secondary = PrimaryLight,
    background = PrimaryDark,
    surface = Primary,
    onSurface = PrimaryLight
)

@SuppressLint("ConflictingOnColor")
private val LightColorPalette = lightColorScheme(
    primary = Primary,
    secondary = PrimaryLight,
    background = PrimaryDark,
    surface = Primary,
    onSurface = PrimaryLight
)

@Composable
fun SolarusTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colorScheme = colors,
        typography = SolarusTypography,
        shapes = Shapes,
        content = content
    )
}