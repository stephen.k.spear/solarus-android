package org.solarus_games.solarus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import org.solarus_games.solarus.common.Constants.GAMES_INDEX
import org.solarus_games.solarus.common.Constants.HOME_INDEX
import org.solarus_games.solarus.common.Constants.INDEX_KEY
import org.solarus_games.solarus.common.Constants.NAVIGATION_KEY
import org.solarus_games.solarus.common.Constants.SETTINGS_INDEX
import org.solarus_games.solarus.common.Constants.SHOP_INDEX
import org.solarus_games.solarus.databinding.MainFragmentBinding
import org.solarus_games.solarus.home.HomeFragment
import org.solarus_games.solarus.preferences.PreferencesFragment
import org.solarus_games.solarus.quest.QuestListFragment
import org.solarus_games.solarus.shop.ShopFragment

class MainFragment : Fragment() {

    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater)
        with(binding) {
            navigation.setOnItemSelectedListener {
                when (it.itemId) {
                    R.id.nav_home -> viewPager.setCurrentItem(HOME_INDEX, false)
                    R.id.nav_games -> viewPager.setCurrentItem(GAMES_INDEX, false)
                    R.id.nav_shop -> viewPager.setCurrentItem(SHOP_INDEX, false)
                    R.id.nav_settings -> viewPager.setCurrentItem(SETTINGS_INDEX, false)
                }
                return@setOnItemSelectedListener true
            }
            setFragmentResultListener(NAVIGATION_KEY) { _, bundle ->
                val result = bundle.getInt(INDEX_KEY)
                when (result) {
                    GAMES_INDEX -> navigation.selectedItemId = R.id.nav_games
                    SHOP_INDEX -> navigation.selectedItemId = R.id.nav_shop
                    SETTINGS_INDEX -> navigation.selectedItemId = R.id.nav_settings
                    else -> navigation.selectedItemId = R.id.nav_home
                }
            }
            with(viewPager) {
                orientation = ViewPager2.ORIENTATION_HORIZONTAL
                isUserInputEnabled = false
                adapter = Adapter(requireActivity().supportFragmentManager, requireActivity().lifecycle).also {
                    with(it) {
                        addFragment(HomeFragment())
                        addFragment(QuestListFragment())
                        addFragment(ShopFragment())
                        addFragment(PreferencesFragment())
                    }
                }
            }
        }
        return binding.root
    }
}

class Adapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val fragmentList = ArrayList<Fragment>()
    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }

    fun addFragment(fragment: Fragment) {
        fragmentList.add(fragment)
    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }
}