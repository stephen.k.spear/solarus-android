package org.solarus_games.solarus.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.os.Parcelable
import android.util.TypedValue
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.view.ContextThemeWrapper
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.background
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.unit.IntSize
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import dagger.hilt.android.internal.managers.ViewComponentManager
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.callbackFlow
import java.util.concurrent.TimeUnit

inline fun <reified T : Parcelable> Intent.parcelable(key: String): T? = when {
    Build.VERSION.SDK_INT >= 33 -> getParcelableExtra(key, T::class.java)
    else -> @Suppress("DEPRECATION") getParcelableExtra(key) as? T
}

/**
 * Converts density independent pixel to real screen pixel. 160 dip = 1 inch
 * ~ 2.5 cm
 *
 * @param dipValue
 * dip
 * @return pixel
 */
fun getPixels(r: Resources, dipValue: Double): Int {
    val dValue = dipValue.toInt()
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dValue.toFloat(), r.displayMetrics).toInt()
}

fun getPixels(v: View, dipValue: Double): Int {
    return getPixels(v.resources, dipValue)
}

fun getPixels(v: Activity, dipValue: Double): Int {
    return getPixels(v.resources, dipValue)
}

/**
 * Moves a view to a screen position. Position is from 0 to 1 and converted
 * to screen pixel. Alignment is top left.
 *
 * @param view
 * View to move
 * @param x
 * X position from 0 to 1
 * @param y
 * Y position from 0 to 1
 */
fun setLayoutPosition(a: Activity, view: View, x: Double, y: Double) {
    val displayMetrics = a.resources.displayMetrics
    val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
    val screenHeightDp = displayMetrics.heightPixels / displayMetrics.density
    val params = RelativeLayout.LayoutParams(
        RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT
    )
    params.leftMargin = getPixels(a, screenWidthDp * x)
    params.topMargin = getPixels(a, screenHeightDp * y)
    view.layoutParams = params
}

/**
 * Moves a view to a screen position. Position is from 0 to 1 and converted
 * to screen pixel. Alignment is top right.
 *
 * @param view
 * View to move
 * @param x
 * X position from 0 to 1
 * @param y
 * Y position from 0 to 1
 */
fun setLayoutPositionRight(a: Activity, view: View, x: Double, y: Double) {
    val displayMetrics = a.resources.displayMetrics
    val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
    val screenHeightDp = displayMetrics.heightPixels / displayMetrics.density
    val params = RelativeLayout.LayoutParams(
        RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT
    )
    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 1)
    params.rightMargin = getPixels(a, screenWidthDp * x)
    params.topMargin = getPixels(a, screenHeightDp * y)
    view.layoutParams = params
}

fun getUIPainter(): Paint? {
    val uiPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    uiPaint.color = Color.argb(128, 255, 255, 255)
    uiPaint.style = Paint.Style.STROKE
    uiPaint.strokeWidth = 3.0.toFloat()
    return uiPaint
}

fun showWrongAPIVersion(context: Context?) {
    Toast.makeText(context, "Not avaible on this API", Toast.LENGTH_SHORT).show()
}

fun Context.getSupportFragmentManager(): FragmentManager? {
    if (this is ViewComponentManager.FragmentContextWrapper) {
        val baseContext = baseContext
        if (baseContext is FragmentActivity) {
            return baseContext.supportFragmentManager
        }
        if (baseContext is ViewComponentManager.FragmentContextWrapper) {
            return baseContext.getSupportFragmentManager()
        }
    }
    if (this is ContextThemeWrapper) {
        val baseContext = baseContext
        if (baseContext is FragmentActivity) {
            return baseContext.supportFragmentManager
        }
        if (baseContext is ViewComponentManager.FragmentContextWrapper) {
            return baseContext.getSupportFragmentManager()
        }
    }
    if (this is FragmentActivity) {
        return supportFragmentManager
    }
    return null
}

fun Context.popBackstack() {
    if (this is ViewComponentManager.FragmentContextWrapper) {
        val baseActivity = baseContext
        if (baseActivity is Activity) {
            baseActivity.getSupportFragmentManager()?.popBackStack()
        }
    }
    if (this is ContextThemeWrapper) {
        val baseContext = baseContext
        if (baseContext is FragmentActivity) {
            baseContext.supportFragmentManager.popBackStack()
        }
        if (baseContext is ViewComponentManager.FragmentContextWrapper) {
            baseContext.getSupportFragmentManager()?.popBackStack()
        }
    }
    if (this is Activity) {
        getSupportFragmentManager()?.popBackStack()
    }
}

fun SharedPreferences.getBooleanFlow(keyFor: String, defaultValue: Boolean = false) =
    callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (keyFor == key) trySend(getBoolean(key, defaultValue))
        }
        registerOnSharedPreferenceChangeListener(listener)
        send(getBoolean(keyFor, defaultValue))
        awaitClose { unregisterOnSharedPreferenceChangeListener(listener) }
    }.buffer(Channel.UNLIMITED)

fun SharedPreferences.getIntFlow(keyFor: String, defaultValue: Int = 0) =
    callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (keyFor == key) trySend(getInt(key, defaultValue))
        }
        registerOnSharedPreferenceChangeListener(listener)
        send(getInt(keyFor, defaultValue))
        awaitClose { unregisterOnSharedPreferenceChangeListener(listener) }
    }.buffer(Channel.UNLIMITED)

fun SharedPreferences.getStringFlow(keyFor: String, defaultValue: String? = null) =
    callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (keyFor == key) trySend(getString(key, defaultValue))
        }
        registerOnSharedPreferenceChangeListener(listener)
        send(getString(keyFor, defaultValue))
        awaitClose { unregisterOnSharedPreferenceChangeListener(listener) }
    }.buffer(Channel.UNLIMITED)

fun Context.openUrlInExternalBrowser(url: String) {
    val intent = Intent(Intent.ACTION_VIEW)
        .setData(Uri.parse(url))
        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
}

fun Long.isValidTimestamp(): Boolean {
    return plus(
        TimeUnit.MILLISECONDS.convert(
            1,
            TimeUnit.DAYS
        )
    ) > System.currentTimeMillis()
}

fun Modifier.shimmerEffect(): Modifier = composed {
    var size by remember {
        mutableStateOf(IntSize.Zero)
    }
    val transition = rememberInfiniteTransition(label = "Shimmer")
    val startOffsetX by transition.animateFloat(
        initialValue = -2 * size.width.toFloat(),
        targetValue = 2 * size.width.toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(1000)
        ), label = "Shimmer"
    )

    background(
        brush = Brush.linearGradient(
            colors = listOf(
                androidx.compose.ui.graphics.Color(0xFFB8B5B5),
                androidx.compose.ui.graphics.Color(0xFF8F8B8B),
                androidx.compose.ui.graphics.Color(0xFFB8B5B5),
            ),
            start = Offset(startOffsetX, 0f),
            end = Offset(startOffsetX + size.width.toFloat(), size.height.toFloat())
        )
    )
        .onGloballyPositioned {
            size = it.size
        }
}

private const val ANIMATION_TIME = 250

val slideInRight = slideInHorizontally(
    initialOffsetX = { 500 }, // small slide 300px
    animationSpec = tween(
        durationMillis = ANIMATION_TIME,
        easing = LinearEasing // interpolator
    )
)

val slideOutRight = slideOutHorizontally(
    targetOffsetX = { 500 },
    animationSpec = tween(
        durationMillis = ANIMATION_TIME,
        easing = LinearEasing
    )
)