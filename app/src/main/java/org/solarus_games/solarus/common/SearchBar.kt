package org.solarus_games.solarus.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.theme.SolarusTypography

@Composable
fun SearchBar(modifier: Modifier, search: String, updateSearch: (search: String) -> Unit, closeSearch: () -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusRequester = remember { FocusRequester() }
    OutlinedTextField(
        colors = OutlinedTextFieldDefaults.colors(
            focusedContainerColor = Color.White,
            focusedBorderColor = Color.Transparent,
            unfocusedBorderColor = Color.Transparent
        ),
        textStyle = SolarusTypography.bodyLarge.copy(color = Color.Black),
        value = search,
        onValueChange = { updateSearch(it) },
        leadingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.search),
                "Search Icon",
                tint = Primary
            )
        },
        trailingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.close),
                "Close Icon",
                modifier = Modifier.clickable {
                    updateSearch("")
                    closeSearch()
                }
            )
        },
        placeholder = {
            Text(
                stringResource(id = R.string.search),
                color = SecondaryTextLight,
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(start = 5.dp)
            )
        },
        singleLine = true,
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done,
            capitalization = KeyboardCapitalization.Words
        ),
        keyboardActions = KeyboardActions(
            onDone = { keyboardController?.hide() }
        ),
        modifier = modifier
            .focusRequester(focusRequester)
            .padding(horizontal = 20.dp)
            .padding(bottom = 20.dp)
            .clip(CircleShape)
    )

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }
}