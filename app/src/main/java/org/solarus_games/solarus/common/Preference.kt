package org.solarus_games.solarus.common

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material.ContentAlpha
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@Composable
fun Preference(
    @StringRes title: Int,
    message: String? = null,
    @DrawableRes icon: Int? = null,
    enabled: Boolean = true,
    onClick: () -> Unit = {}
) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(20.dp)
    ) {
        icon?.let {
            Image(
                painter = painterResource(id = icon), contentDescription = "icon",
                modifier = Modifier
                    .size(24.dp)
                    .alpha(if (enabled) ContentAlpha.medium else ContentAlpha.disabled)
            )
        }
        Column(
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalAlignment = Alignment.Start,
            modifier = Modifier.weight(1f)
        ) {
            PreferenceHeader(stringResource(id = title), enabled)
            message?.let {
                PreferenceMessage(it, enabled)
            }
        }
    }
}