package org.solarus_games.solarus.common

import android.util.Log
import org.solarus_games.solarus.BuildConfig
import javax.inject.Inject

class Logger @Inject constructor() {

    fun e(tag: String? = null, message: String? = null, exception: Throwable? = null) {
        if (BuildConfig.DEBUG)  Log.e(tag, message, exception)
    }

    fun d(tag: String?, message: String) {
        if (BuildConfig.DEBUG)  Log.d(tag, message)
    }
}