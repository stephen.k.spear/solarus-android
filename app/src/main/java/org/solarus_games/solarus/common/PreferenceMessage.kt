package org.solarus_games.solarus.common

import androidx.compose.foundation.layout.padding
import androidx.compose.material.ContentAlpha
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.solarus_games.solarus.theme.SecondaryTextDark

@Composable
fun PreferenceMessage(text: String, enabled: Boolean = true) {
    Text(
        text,
        textAlign = TextAlign.Start,
        fontFamily = FontFamily.Serif,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        fontSize = 14.sp,
        fontWeight = FontWeight.Normal,
        color = SecondaryTextDark,
        modifier = Modifier.padding(start = 2.dp).alpha( if (enabled) ContentAlpha.medium else ContentAlpha.disabled),
    )
}