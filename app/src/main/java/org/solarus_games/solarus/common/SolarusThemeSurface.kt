package org.solarus_games.solarus.common

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.rememberNestedScrollInteropConnection
import org.solarus_games.solarus.theme.SolarusTheme

@Composable
fun SolarusThemeSurface(
    content: @Composable () -> Unit,
) {
    SolarusTheme {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .nestedScroll(rememberNestedScrollInteropConnection()),
            color = MaterialTheme.colorScheme.background,
        ) {
            content()
        }
    }
}