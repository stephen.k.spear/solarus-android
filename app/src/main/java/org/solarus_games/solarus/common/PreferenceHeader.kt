package org.solarus_games.solarus.common

import androidx.compose.material.ContentAlpha
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import org.solarus_games.solarus.theme.SecondaryTextLight

@Composable
fun PreferenceHeader(text: String, enabled: Boolean = true) {
    Text(
        text,
        textAlign = TextAlign.Start,
        fontFamily = FontFamily.Serif,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        fontSize = 18.sp,
        fontWeight = FontWeight.Thin,
        color = SecondaryTextLight,
        modifier = Modifier.alpha( if (enabled) ContentAlpha.medium else ContentAlpha.disabled)
    )
}