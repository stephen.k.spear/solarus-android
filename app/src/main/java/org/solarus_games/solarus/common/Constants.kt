package org.solarus_games.solarus.common

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.solarus_games.solarus.R

object Constants {
    const val SOLARUS_BASE_URL = "https://www.solarus-games.org/"
    const val SOLARUS_GAMES_ENDPOINT = "games/index.json"
    const val SOLARUS_NEWS_ENDPOINT = "news/index.json"

    const val CHANGE_LOG_URL = "https://gitlab.com/solarus-games/solarus-android/-/releases"
    const val SOURCE_CODE_URL = "https://gitlab.com/solarus-games/solarus-android"
    const val REPORT_BUG_URL = "https://gitlab.com/solarus-games/solarus-android/-/issues"

    const val BUTTON_TEXT_PADDING = 10
    const val COLUMN_PADDING = 30
    const val SETTING_SPACING = 25
    const val LARGE_CARD_CORNERS = 10

    const val NAVIGATION_KEY = "navigation"
    const val INDEX_KEY = "index"

    const val HOME_INDEX = 0
    const val GAMES_INDEX = 1
    const val SHOP_INDEX = 2
    const val SETTINGS_INDEX = 3

    val moshiBuild: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    inline fun <reified T> moshi(): JsonAdapter<T> = moshiBuild.adapter(T::class.java)
    inline fun <reified T, reified D> moshiList(): JsonAdapter<T> =
        moshiBuild.adapter(Types.newParameterizedType(List::class.java, D::class.java))
}

enum class QuestProperties {
    DEVELOPER,
    RELEASE_DATE,
    LAST_UPDATE,
    LICENSES,
    AGE_RATING,
    LANGUAGE,
    CONTROLS,
    GENRE,
    VERSION,
    PLATFORM,
    SOURCE_CODE,
    WEBSITE
}

enum class Languages(@DrawableRes val icon: Int) {
    DE(R.drawable.de),
    EN(R.drawable.en),
    ES(R.drawable.es),
    FR(R.drawable.fr),
    IT(R.drawable.it),
    PT(R.drawable.pt),
    RU(R.drawable.ru),
    ZH(R.drawable.zh)
}

enum class Controls(@DrawableRes val icon: Int) {
    KEYBOARD(R.drawable.keyboard),
    MOUSE(R.drawable.mouse),
    GAMEPAD(R.drawable.gamepad),;
}

enum class Ratings(@StringRes val value: Int) {
    ALL(R.string.rating_all),
}
