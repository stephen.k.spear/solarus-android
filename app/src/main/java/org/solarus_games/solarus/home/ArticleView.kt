package org.solarus_games.solarus.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.SubcomposeAsyncImage
import org.solarus_games.solarus.common.Constants.LARGE_CARD_CORNERS
import org.solarus_games.solarus.common.Constants.SOLARUS_BASE_URL
import org.solarus_games.solarus.remote.models.Article
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.SecondaryText
import org.solarus_games.solarus.theme.SolarusTypography
import org.solarus_games.solarus.utils.openUrlInExternalBrowser
import org.solarus_games.solarus.utils.shimmerEffect

@Composable
fun ArticleView(
    modifier: Modifier,
    article: Article,
) {
    val context = LocalContext.current
    with(article) {
        Card(
            onClick = {
                context.openUrlInExternalBrowser(SOLARUS_BASE_URL.plus(permalink))
            },
            shape = RoundedCornerShape(LARGE_CARD_CORNERS.dp),
            elevation = CardDefaults.cardElevation(
                defaultElevation = 15.dp,
                pressedElevation = 0.dp,
                hoveredElevation = 8.dp
            ),
            modifier = modifier
        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                if (thumbnail.isNotEmpty()) {
                    SubcomposeAsyncImage(
                        model = SOLARUS_BASE_URL.plus(thumbnail),
                        loading = {
                            Box(
                                Modifier
                                    .background(Color.Blue)
                                    .fillMaxSize()
                                    .shimmerEffect()
                            )
                        },
                        contentDescription = title,
                        modifier = Modifier
                            .fillMaxWidth()
                            .align(Alignment.TopCenter),
                        contentScale = ContentScale.FillWidth
                    )
                }

                Column(
                    modifier = Modifier
                        .align(Alignment.BottomCenter)
                        .fillMaxWidth()
                        .background(Primary)
                        .padding(15.dp),
                    horizontalAlignment = Alignment.Start,
                    verticalArrangement = Arrangement.spacedBy(5.dp)
                ) {
                    Text(title, style = SolarusTypography.bodyLarge, maxLines = 2, color = SecondaryText)
                    Text(date, style = SolarusTypography.labelSmall, maxLines = 1, color = SecondaryText)
                    Text(excerpt, style = SolarusTypography.bodyMedium, maxLines = 2, color = SecondaryText)
                }
            }
        }
    }
}