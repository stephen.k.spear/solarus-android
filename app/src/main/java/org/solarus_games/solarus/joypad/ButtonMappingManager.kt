package org.solarus_games.solarus.joypad

import android.app.Application
import android.content.Context
import android.util.Log
import android.view.KeyEvent
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.solarus_games.solarus.utils.FileHelper
import java.io.BufferedWriter
import java.io.IOException
import java.io.OutputStreamWriter
import java.util.LinkedList
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ButtonMappingManager @Inject constructor(private val context: Application) {

    private val layoutList = mutableListOf<InputLayout>()
    var defaultLayoutId = 0
        private set

    init {
        readButtonMappingFile()
    }

    /**
     * Add an input layout, add it as the default one if the list was empty
     */
    fun add(p: InputLayout) {
        layoutList.add(p)

        // Set the default layout if there is no one
        if (layoutList.size == 1) {
            setDefaultLayout(p.id)
        }
    }

    /**
     * Delete safely a layout : handle problems of empty layout list and suppression of default layout
     */
    fun delete(context: Context?, p: InputLayout) {
        // Remove p
        layoutList.remove(p)

        // If p was the last layout : add the default layout
        if (layoutList.size <= 0) {
            add(InputLayout.getDefaultInputLayout(context))
        }

        // If p was the default layout : the first layout become the new default layout
        if (p.id == defaultLayoutId) {
            defaultLayoutId = layoutList[0].id
        }

        // Save the result
        save()
    }

    fun getLayoutById(id: Int): InputLayout? {
        // The layout exist : return it
        for (i in layoutList) {
            if (i.id == id) return i
        }

        // The layout doesn't exist : return the default one
        return getLayoutById(defaultLayoutId)
    }

    fun setDefaultLayout(id: Int) {
        // TODO : Verify if the id is in the input layout's list
        defaultLayoutId = id
    }

    fun getLayoutList(): List<InputLayout> {
        return layoutList
    }

    val layoutsNames: Array<String?>
        /**
         * Return the list of the input layout's name
         */
        get() {
            val layoutNameArray = arrayOfNulls<String>(layoutList.size)
            for (i in layoutNameArray.indices) {
                layoutNameArray[i] = layoutList[i].name
            }
            return layoutNameArray
        }

    /**
     * Convert the current input layout model in a JSON Object
     */
    private fun serialize(): JSONObject {
        val o = JSONObject()
        try {
            val presets = JSONArray()
            for (p in layoutList) {
                presets.put(p.serialize())
            }
            o.put(TAG_VERSION, NUM_VERSION)
            o.put(TAG_DEFAULT_LAYOUT, defaultLayoutId)
            o.put(TAG_PRESETS, presets)
        } catch (e: JSONException) {
            Log.e("Button Maping Model", "Impossible to serialize the button mapping model")
        }
        return o
    }

    fun save() {
        writeButtonMappingFile()
    }


    /**
     * Generate all valid key codes
     * @return
     */
    fun generateKeyCodes(): List<Int> {
        val code: MutableList<Int> = ArrayList()
        for (i in KeyEvent.KEYCODE_0 until KeyEvent.KEYCODE_CTRL_RIGHT) {
            code.add(i)
        }
        return code
    }


    fun keycodeFromString(str: String?): Int {
        return KeyEvent.keyCodeFromString(str!!)
    }

    /**
     * Generate keyCode strings
     * @return
     */
    fun generateKeyCodeStrings(): List<String> {
        val codes = generateKeyCodes()
        val strings: MutableList<String> = ArrayList()
        for (i in codes) {
            strings.add(keycodeToString(i))
        }
        return strings
    }

    /**
     * Return the default Button Mapping model
     */
    private fun getDefaultButtonMapping(context: Context) {
        add(InputLayout.getDefaultInputLayout(context))
        defaultLayoutId = 0
    }

    private fun readButtonMappingFile() {
        try {
            // Parse the JSON
            val text = FileHelper.readInternalFileContent(context, FILE_NAME)
            val jso = FileHelper.readJSON(text)
            if (jso == null) {
                Log.i("Button Mapping Model", "No $FILE_NAME found, loading the default Button Mapping System")
                getDefaultButtonMapping(context)
                return
            }

            val layoutArray = jso.getJSONArray("presets")
            var p: JSONObject
            for (i in 0 until layoutArray.length()) {
                p = layoutArray[i] as JSONObject
                InputLayout.deserialize(context, p)?.let { add(it) }
            }

            setDefaultLayout(jso.getInt(TAG_DEFAULT_LAYOUT))
        } catch (e: JSONException) {
            Log.e("Button Mapping Model", "Error parsing the Button Mapping file, loading the default one")
            getDefaultButtonMapping(context)
        }
    }

    private fun writeButtonMappingFile() {
        try {
            // FileWriter file = new FileWriter(button_mapping_path);
            val fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val file = BufferedWriter(OutputStreamWriter(fos))
            val obj = serialize()
            file.write(obj.toString(2))
            file.flush()
            file.close()
            Log.i("Button Mapping Model", "File writed with success")
        } catch (e: IOException) {
            Log.e("Button Mapping Model", "Error writting the button mapping file")
        } catch (e: JSONException) {
            Log.e("Button Mapping Model", "Error parsing the button mapping file for writing")
        }
    }

    class InputLayout(@JvmField var name: String) {
        var id: Int
            private set

        var buttonList: MutableList<VirtualButton> = ArrayList()


        init {
            // TODO : Verify that id hasn't been already taken (like 0,00000001% probability)
            id = (Math.random() * 100000000).toInt()
        }

        constructor(name: String, id: Int) : this(name) {
            this.id = id
        }

        fun add(v: VirtualButton) {
            buttonList.add(v)
        }

        fun serialize(): JSONObject? {
            try {
                val preset = JSONObject()
                preset.put(TAG_NAME, name)
                preset.put(TAG_ID, id)

                // Circle/Buttons
                val layout_array = JSONArray()
                for (button in buttonList) {
                    val jso = JSONObject()
                    jso.put(TAG_KEYCODE, button.getKeyCode())
                    jso.put(TAG_X, button.getPosX())
                    jso.put(TAG_Y, button.getPosY())
                    jso.put(TAG_SIZE, button.size)
                    layout_array.put(jso)
                }
                preset.put(TAG_BUTTONS, layout_array)
                return preset
            } catch (e: JSONException) {
                Log.e("Button Mapping File", "Error while serializing an input layout : " + e.message)
            }
            return null
        }

        fun isDefaultInputLayout(bmm: ButtonMappingManager): Boolean {
            return id == bmm.defaultLayoutId
        }


        companion object {
            fun deserialize(context: Context?, jso: JSONObject): InputLayout? {
                try {
                    val layout = InputLayout(jso.getString(TAG_NAME), jso.getInt(TAG_ID))
                    val button_list = jso.getJSONArray(TAG_BUTTONS)
                    for (i in 0 until button_list.length()) {
                        val button = button_list[i] as JSONObject
                        val keyCode = button.getInt(TAG_KEYCODE)
                        val size = button.getInt(TAG_SIZE)
                        val posX = button.getDouble(TAG_X)
                        val posY = button.getDouble(TAG_Y)
                        if (keyCode == VirtualButton.DPAD) {
                            layout.add(VirtualCross(context, posX, posY, size))
                        } else {
                            layout.add(VirtualButton.Create(context, keyCode, posX, posY, size))
                        }
                    }
                    return layout
                } catch (e: JSONException) {
                    Log.e("Button Mapping File", "Error while deserializing an input layout : " + e.message)
                }
                return null
            }

            /**
             * Return the default button mapping preset : one cross, two buttons
             */
            @JvmStatic
            fun getDefaultInputLayout(context: Context?): InputLayout {
                val b = InputLayout(DEFAULT_NAME, 0)
                b.buttonList = getDefaultButtonList(context)
                return b
            }

            @JvmStatic
            fun getDefaultButtonList(context: Context?): LinkedList<VirtualButton> {
                val l = LinkedList<VirtualButton>()
                l.add(VirtualCross(context, 0.0, 0.5, 100))
                l.add(VirtualButton.Create(context, KeyEvent.KEYCODE_X, 0.65, 0.65, 100))
                l.add(VirtualButton.Create(context, KeyEvent.KEYCODE_C, 0.75, 0.8, 100))
                l.add(VirtualButton.Create(context, KeyEvent.KEYCODE_V, 0.75, 0.5, 100))
                l.add(VirtualButton.Create(context, KeyEvent.KEYCODE_SPACE, 0.85, 0.65, 100))
                l.add(VirtualButton.Create(context, KeyEvent.KEYCODE_D, 0.45, 0.8, 100))
                //l.add(new MenuButton(context, 0, 0, 90));
                return l
            }
        }
    }

    companion object {
        const val NUM_VERSION = 1
        const val TAG_VERSION = "version"
        const val TAG_PRESETS = "presets"
        const val TAG_DEFAULT_LAYOUT = "default"
        const val DEFAULT_NAME = "Solarus Team"
        const val TAG_ID = "id"
        const val TAG_NAME = "name"
        const val TAG_BUTTONS = "buttons"
        const val TAG_KEYCODE = "keycode"
        const val TAG_X = "x"
        const val TAG_Y = "y"
        const val TAG_SIZE = "size"
        const val FILE_NAME = "button_mapping.txt"

        /**
         * Transform key code in strings
         * @param keycode
         * @return
         */
        @JvmStatic
        fun keycodeToString(keycode: Int): String {
            val r = KeyEvent.keyCodeToString(keycode)
            return r.replace("KEYCODE_", "")
        }
    }
}