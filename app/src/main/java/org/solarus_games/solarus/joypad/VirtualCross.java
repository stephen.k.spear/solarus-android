package org.solarus_games.solarus.joypad;

import static org.solarus_games.solarus.utils.UiHelperKt.getPixels;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.MotionEvent;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.libsdl.app.SDLActivity;

public class VirtualCross extends VirtualButton {
    private Rect boundLeft, boundRight, boundUp, boundDown;
    //private int key_pressed;
    private SparseArray<PointF> mActivePointers = new SparseArray<>();
    private Set<Integer> keys_pressed = new HashSet<>();
    private Path path = new Path(); // For the drawing

    private boolean hasVibrate;

    public VirtualCross(Context context, double posX, double posY, int size) {
        super(context, VirtualButton.DPAD, posX, posY, size);

        // Base size: ~1 cm
        originalSize = getPixels(this, 150);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!debug_mode) {
            painter.setAlpha(preferencesManager.getLayoutTransparency());
        }

        int iconSize_33 = (int) (realSize * 0.33);
        // Draw the cross
        path.reset();
        path.moveTo(iconSize_33, 5);
        path.lineTo(iconSize_33 * 2, 5);
        path.lineTo(iconSize_33 * 2, iconSize_33);
        path.lineTo(realSize - 5, iconSize_33);
        path.lineTo(realSize - 5, iconSize_33 * 2);
        path.lineTo(iconSize_33 * 2, iconSize_33 * 2);
        path.lineTo(iconSize_33 * 2, realSize - 5);
        path.lineTo(iconSize_33, realSize - 5);
        path.lineTo(iconSize_33, iconSize_33 * 2);
        path.lineTo(5, iconSize_33 * 2);
        path.lineTo(5, iconSize_33);
        path.lineTo(iconSize_33, iconSize_33);
        path.close();
        path.offset(0, 0);
        canvas.drawPath(path, painter);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (debug_mode) {
            ButtonMappingActivity.dragVirtualButton(this, event);
        } else {
            setBounds();
            int action = event.getActionMasked();

            // get pointer index from the event object
            int pointerIndex = event.getActionIndex();

            // get pointer ID
            int pointerId = event.getPointerId(pointerIndex);

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    if(preferencesManager.isVibrationEnabled()) {
                        vibrator.vibrate(preferencesManager.getVibrationDuration());
                    }
                case MotionEvent.ACTION_POINTER_DOWN:
                    PointF f = new PointF();
                    f.x = getLeft() + event.getX(pointerIndex);
                    f.y = getTop() + event.getY(pointerIndex);
                    mActivePointers.put(pointerId, f);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mActivePointers.remove(pointerId);
                    break;
                case MotionEvent.ACTION_MOVE:
                    for (int size = event.getPointerCount(), i = 0; i < size; i++) {
                        PointF point = mActivePointers.get(event.getPointerId(i));
                        if (point != null) {
                            point.x = getLeft() + event.getX(i);
                            point.y = getTop() + event.getY(i);
                        }
                    }
                    break;
                default:
                    return true;
            }


            //Compute keys that are actually pressed
            HashSet<Integer> keys_now = new HashSet<>();

            for (int size = mActivePointers.size(), i = 0; i < size; i++) {
                PointF point = mActivePointers.valueAt(i);
                int x = (int)point.x;
                int y = (int)point.y;

                if (boundLeft.contains(x, y)) {
                    keys_now.add(KeyEvent.KEYCODE_DPAD_LEFT);
                }
                if (boundRight.contains(x,y)) {
                    keys_now.add(KeyEvent.KEYCODE_DPAD_RIGHT);
                }
                if (boundUp.contains(x,y)) {
                    keys_now.add(KeyEvent.KEYCODE_DPAD_UP);
                }
                if (boundDown.contains(x,y)) {
                    keys_now.add(KeyEvent.KEYCODE_DPAD_DOWN);
                }

            }

            for(Integer key_code : Arrays.asList(KeyEvent.KEYCODE_DPAD_LEFT,
                    KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN)) {
                handleKeycode(key_code, keys_now);
            }

            keys_pressed = keys_now;

        }
        return true;
    }

    /**
     * Send sdl message if this key state changes
     *
     * @param keycode key
     * @param pressed actual key state
     */
    private void handleKeycode(int keycode, Set<Integer> pressed) {
        boolean before = keys_pressed.contains(keycode);
        boolean now = pressed.contains(keycode);

        if(before && !now) { //Key released
            sendSDLUpMessage(keycode);
            if(preferencesManager.isVibrateDpad()) { //TODO slide vibration
                vibrator.vibrate(preferencesManager.getVibrationDuration());
            }
        } else if(!before && now) { //Key pressed
            sendSDLDownMessage(keycode);
            if(preferencesManager.isVibrateDpad()) {
                vibrator.vibrate(preferencesManager.getVibrationDuration());
            }
        }
    }

    public void sendSDLDownMessage(int keycode) {
        //Log.i("Cross", keycode + " pressed.");
        SDLActivity.onNativeKeyDown(keycode);
    }

    public void sendSDLUpMessage(int keycode) {
        // Log.i("Cross", keycode + " released.");
        SDLActivity.onNativeKeyUp(keycode);
    }

    /**
     * Set the direction's hitbox position
     */
    public void setBounds() {
        int iconSize_33 = (int) (realSize * 0.33);
        int padding = (int) (realSize * 0.20); // We use it to slightly increase
        // hitboxs

        //Multidirs bounds
        boundLeft = new Rect(this.getLeft() - padding, this.getTop(), this.getRight() - 2 * iconSize_33,
                this.getBottom() + padding);
        boundRight = new Rect(this.getLeft() + 2 * iconSize_33, this.getTop(), this.getRight() + padding,
                this.getBottom() + padding);
        boundUp = new Rect(this.getLeft(), this.getTop() - padding, this.getRight(),
                this.getBottom() - 2 * iconSize_33);
        boundDown = new Rect(this.getLeft(), this.getTop() + 2 * iconSize_33,
                this.getRight() , this.getBottom() + padding);
    }
}
