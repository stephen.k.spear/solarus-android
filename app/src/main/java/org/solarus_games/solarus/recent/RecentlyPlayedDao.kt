package org.solarus_games.solarus.recent

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow


@Dao
interface RecentlyPlayedDao {
    @Query("SELECT * FROM recently_played ORDER BY last_played DESC LIMIT 3")
    fun getRecentGames(): Flow<List<RecentlyPlayed>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(game: RecentlyPlayed)
}