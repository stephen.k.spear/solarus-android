package org.solarus_games.solarus.remote

import okhttp3.ResponseBody
import org.solarus_games.solarus.common.Constants.SOLARUS_GAMES_ENDPOINT
import org.solarus_games.solarus.common.Constants.SOLARUS_NEWS_ENDPOINT
import org.solarus_games.solarus.remote.models.GameResponse
import org.solarus_games.solarus.remote.models.NewsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface RemoteService {

    @GET(SOLARUS_GAMES_ENDPOINT)
    suspend fun getGames(): GameResponse

    @GET(SOLARUS_NEWS_ENDPOINT)
    suspend fun getNews(): NewsResponse

    @Streaming
    @GET
    suspend fun downloadGame(@Url fileUrl:String): Response<ResponseBody>
}