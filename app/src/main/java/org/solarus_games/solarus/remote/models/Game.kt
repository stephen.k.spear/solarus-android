package org.solarus_games.solarus.remote.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Game(
    val age: String,
    val controls: List<String>,
    val developer: List<String>,
    val download: String,
    val downloadSize: Int,
    val excerpt: String,
    val genre: List<String>,
    val id: String,
    val initialReleaseDate: String,
    val languages: List<String>,
    val latestUpdateDate: String,
    val licenses: List<String>,
    val maximumPlayers: Int,
    val minimumPlayers: Int,
    val page: String,
    val platforms: List<String>,
    val solarusVersion: String,
    val thumbnail: String,
    val title: String,
    val version: String,
    val website: String
) : Parcelable