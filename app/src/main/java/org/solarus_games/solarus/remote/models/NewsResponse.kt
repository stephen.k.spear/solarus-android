package org.solarus_games.solarus.remote.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewsResponse(
    val data: List<Article>,
    val version: String,
    val timestamp: Long = System.currentTimeMillis()
)