package org.solarus_games.solarus.remote.models

import androidx.compose.runtime.Immutable
import org.solarus_games.solarus.quest.Quest

@Immutable
data class HomeScreen(
    val loading: Boolean = false,
    val recents: List<Quest> = emptyList(),
    val news: List<Article> = emptyList()
)
