package org.solarus_games.solarus.remote.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GameResponse(
    val data: List<Game>,
    val version: String,
    val timestamp: Long = System.currentTimeMillis()
)