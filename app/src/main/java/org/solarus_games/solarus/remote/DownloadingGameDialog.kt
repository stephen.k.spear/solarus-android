package org.solarus_games.solarus.remote

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.common.ComposeDialog

@AndroidEntryPoint
class DownloadingGameDialog : ComposeDialog() {

    @Composable
    override fun GetContent() {
        Column(modifier = Modifier.padding(vertical = 50.dp), verticalArrangement = Arrangement.spacedBy(20.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            CircularProgressIndicator()
            Text("Downloading game, please wait...")
        }
    }

    companion object {
        const val TAG = "DownloadingGameDialog"
        fun newInstance(): DownloadingGameDialog {
            return DownloadingGameDialog()
        }
    }
}