# Solarus Android

This repository contains the Android Studio/Gradle project for building Solarus
and its dependencies with ndkBuild and make a SDLActivity based appplication.

## Project Status

- Working features:
  - Video.
  - Audio.
  - Quest data loading.
  - Save files, write directory.
  - Plain Lua.
  - Touch and Bluetooth joystick.
  - Quest browsing and thumbnails.
  - LuaJIT.
- Non-working features:
  - Some UI parts are broken or missing.

## Dependencies

This repository uses [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) for dependencies.

It is recommended to use the [`--recurse-submodules`](https://git-scm.com/docs/git-clone#Documentation/git-clone.txt---recurse-submodulesltpathspecgt)
argument when [cloning the repository](https://git-scm.com/docs/git-clone).

The submodules can also be [updated and initialized](https://git-scm.com/docs/git-submodule) after cloning:
```
git submodule update --init --recursive
```

When updating dependency submodules, ensure to only checkout TAGGED commits (releases). Do NOT update
dependency submodules to latest commits in branches, even if they are release branches. Updating via
specific repository TAGS is necessary to ensure build stability over time.

For example, to update the SDL2 submodule to the repository tag `release-2.30.1`:
```
git -C app/jni/SDL2 fetch --tags
git -C app/jni/SDL2 checkout release-2.30.1  # this should be a repository TAG, not a branch
```

When updating SDL2, ensure to also update all the files in `app/src/main/java` according to the
updated files in `app/jni/SDL2/android-project/app/src/main/java`.

## Building

The easiest way to build the project is with [Android Studio](https://developer.android.com/studio).

The project can also be build using the basic Android
[command line tools](https://developer.android.com/studio#command-line-tools-only) package.

First, install the Command line tools package and configure the `ANDROID_HOME`
environment variable ([documentation](https://developer.android.com/studio/command-line)).

Then, download and accept all SDK licenses using `sdkmanager`:
```
yes | $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager --licenses
```

And finally, build the project (in _Debug_ mode) using the Gradle Wrapper script:
```
./gradlew :cmake_build:assembleDebug :app:assembleDebug
```

> **Note:** Gradle will automatically download all the necessary SDK components.

The resulting APK file will be in `app/build/outputs/apk/debug/`.
